// AUTO REJECTED PROMISE --------------------------------------------
// let myFirstPromise = new Promise(function (resolve, reject) {
//     resolve("rejected")
// });

// myFirstPromise
//     .then(function (x) {
//         console.log(x)
//     })
//     .catch(function (x) {
//         console.log(x)
//     })

// AUTO RESOLVED PROMISE --------------------------------------------

// let myFirstPromise = new Promise((resolve, reject) => {
//     resolve("resolved")
// });

// myFirstPromise
//     .then(function (x) {
//         console.log(x)
//     })
//     .catch(function (x) {
//         console.log(x)
//     })

// PROMISE BASED ON CONDITION --------------------------------------------
// const condition = false;

// let myFirstPromise = new Promise(function (resolve, reject) {
//     if (condition) {
//         resolve("fulfilled");
//     } else {
//         reject("rejected");
//     }
// })

// myFirstPromise
//     .then(function (x) {
//         console.log(x)
//     })
//     .catch(function (x) {
//         console.log(x)
//     })

// CUSTOM PROMISE --------------------------------------------
// const name = "evan";

// let isImpactByteStudent = new Promise(function (resolve, reject) {
//     if (name === "evan") {
//         resolve(name)
//     } else if (name === "adi") {
//         resolve(name)
//     } else {
//         reject(name)
//     }
// })

// isImpactByteStudent
//     .then(function (x) {
//         console.log(`${x} is Impact Byte student`)
//     })
//     .catch(function (x) {
//         console.log(`${x} is not Impact Byte student`)
//     })

// ADVANCED EXAMPLE --------------------------------------------
// const isMomHappy = false;

// // Promise
// const willIGetNewPhone = new Promise((resolve, reject) => {
//   // fat arrow
//   if (isMomHappy) {
//     const phone = {
//       brand: "Samsung",
//       color: "black"
//     };
//     resolve(phone);
//   } else {
//     const reason = new Error("mom is not happy");
//     reject(reason);
//   }
// });

// const showOff = function(phone) {
//   const message =
//     "Hey friend, I have a new " + phone.color + " " + phone.brand + " phone";
//   return Promise.resolve(message);
// };

// // call our promise
// const askMom = function() {
//   willIGetNewPhone
//     .then(showOff)
//     .then(fulfilled => console.log(fulfilled)) // fat arrow
//     .catch(error => console.log(error.message)); // fat arrow
// };

// askMom();

// PROMISE WITH PARAM  --------------------------------------------
// function isImpactByteStudent(name) {
//     return new Promise(function (resolve, reject) {
//         if (name === "evan") {
//             resolve(name)
//         } else if (name === "adi") {
//             resolve(name)
//         } else {
//             reject(name)
//         }
//     })
// }
// isImpactByteStudent("evan")
//     .then(function (x) {
//         console.log(`${x} is Impact Byte student`)
//     })
//     .catch(function (x) {
//         console.log(`${x} is not Impact Byte student`)
//     })

// PROMISE WITH PARAM USING (Promise.resolve()/ Promise.reject())
// function isImpactByteStudent(name) {
//         if (name === "evan") {
//             return Promise.resolve(name)
//         } else if (name === "adi") {
//             return Promise.resolve(name)
//         } else {
//             return Promise.reject(name)
//         }
// }
// isImpactByteStudent("adi")
//     .then(function (x) {
//         console.log(`${x} is Impact Byte student`)
//     })
//     .catch(function (x) {
//         console.log(`${x} is not Impact Byte student`)
//     })

// CREATING PROMISE WITH ASYNC ----------------------------------

// const myAsyncFunction = async () => {
// 	return "value"
// }

// async function myAsyncFunction() {
//     return "theValue";
// }

// myAsyncFunction().then(X => console.log(X));

//  ASYNC AWAIT EXAMPLE ----------------------------------

// function isImpactByteStudent(name) {
//     if (name === "evan") {
//         return Promise.resolve(name)
//     } else if (name === "adi") {
//         return Promise.resolve(name)
//     } else {
//         return Promise.reject(name)
//     }
// }



// async function checkIsImpactByteStudent() {
//     console.log("before promise")
//     await isImpactByteStudent("adi")
//         .then(function (x) {
//             console.log(`${x} is Impact Byte student`)
//         })
//         .catch(function (x) {
//             console.log(`${x} is not Impact Byte student`)
//         })
//     console.log("after promise promise")
// }

// checkIsImpactByteStudent()

// RETURN VALUE FROM PROMISE ---------------------------------

// function isImpactByteStudent(name) {
//     if (name === "evan") {
//         return Promise.resolve(name)
//     } else if (name === "adi") {
//         return Promise.resolve(name)
//     } else {
//         return Promise.reject(name)
//     }
// }

// async function checkAdi() {
//     var result = await isImpactByteStudent("adi")
//         .then(function (x) {
//             return `${x} is Impact Byte student`
//         })
//         .catch(function (x) {
//             return `${x} is not Impact Byte student`
//         })
//     console.log(result)
// }

// checkAdi()